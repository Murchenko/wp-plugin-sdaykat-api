<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    Sdaykat_Api
 * @subpackage Sdaykat_Api/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Sdaykat_Api
 * @subpackage Sdaykat_Api/admin
 * @author     Your Name <email@example.com>
 */
class Sdaykat_Api_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $sdaykat_api    The ID of this plugin.
	 */
	private $sdaykat_api;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $sdaykat_api       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $sdaykat_api, $version ) {

		$this->sdaykat_api = $sdaykat_api;
		$this->version = $version;
		$this->Sdaykat_Api_Render = new Sdaykat_Api_Render('admin');

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Sdaykat_Api_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Sdaykat_Api_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->sdaykat_api, plugin_dir_url( __FILE__ ) . 'css/sdaykat-api-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Sdaykat_Api_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Sdaykat_Api_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->sdaykat_api, plugin_dir_url( __FILE__ ) . 'js/sdaykat-api-admin.js', array( 'jquery' ), $this->version, false );

	}

	public function plugin_settings(){
		// параметры: $sdaykat-api-settings-group, $settings, $sanitize_callback
		register_setting( 'sdaykat-api-settings-group', 'sdaykat-api-settings', array($this, 'sanitize_callback') );
	
		// параметры: $id, $title, $callback, $page
		add_settings_section( 'sdaykat-api-settings-section', 'Основные настройки', '', 'sdaykat-api-settings' ); 
	
		// параметры: $id, $title, $callback, $page, $section, $args
		add_settings_field('token_field', 'API TOKEN', array($this, 'fill_token_field'), 'sdaykat-api-settings', 'sdaykat-api-settings-section' );
	}

	## Заполняем опцию 1
	public function fill_token_field(){
		$val = get_option('sdaykat-api-settings');
		$val = $val ? $val['token'] : null;
		?>
		<input type="text" name="sdaykat-api-settings[token]" value="<?php echo esc_attr( $val ) ?>" />
		<?php
	}

	## Очистка данных
	public function sanitize_callback( $options ){ 
		// очищаем
		foreach( $options as $name => & $val ){
			if( $name == 'token' )
				$val = strip_tags( $val );

			if( $name == 'checkbox' )
				$val = intval( $val );
		}

		return $options;
	}

	public function add_menu_pages(){
		add_menu_page( 'Sdaykat API', 'Sdaykat API', 'manage_options', 'sdaykat-api', array($this, 'render_geo_page'), plugin_dir_url(__FILE__) . 'img/main_icon.svg?v1');
		add_submenu_page('sdaykat-api', 'Гео', 'Гео', 'manage_options', 'sdaykat-api');
		add_submenu_page('sdaykat-api', 'Регионы', 'Регионы', 'manage_options', 'sdaykat-api-regions', array($this, 'render_regions_page'));
		add_submenu_page('sdaykat-api', 'Настройки', 'Настройки', 'manage_options', 'sdaykat-api-settings', array($this, 'render_options_page'), 99);
	}
	
	public function render_options_page() {
		$this->Sdaykat_Api_Render->tabs();?>
		<form action="options.php" method="POST">
			<?php
				settings_fields( 'sdaykat-api-settings-group' );     // скрытые защитные поля
				do_settings_sections( 'sdaykat-api-settings' ); // секции с настройками (опциями). У нас она всего одна 'sdaykat-api-settings-section'
				submit_button();
			?>
		</form>
		<?
	}

	public function render_geo_page() {

		$this->Sdaykat_Api_Render->tabs();
		$this->Sdaykat_Api_Render->geo_page();

		// var_dump(admin_url());
	}

	public function render_regions_page() {

		$this->Sdaykat_Api_Render->tabs();
		$this->Sdaykat_Api_Render->regions_page();

		// var_dump(admin_url());
	}

}
