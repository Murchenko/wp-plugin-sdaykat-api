<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    Sdaykat_Api
 * @subpackage Sdaykat_Api/admin/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
<div class="nav-tab-wrapper">
    <a class="nav-tab nav-tab-active" href="?page=sdaykat-api">Гео</a>
    <a class="nav-tab " href="?page=sdaykat-api-settings">Настройки</a>
</div>