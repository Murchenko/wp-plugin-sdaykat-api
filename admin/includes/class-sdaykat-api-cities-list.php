<?

if( ! class_exists( 'WP_Users_List_Table' ) ) {
    require_once( ABSPATH . 'wp-admin/includes/class-wp-users-list-table.php' );
}

class Sdaykat_Api_Cities_List extends WP_List_Table {

    private $apiClass;

    public function __construct( $args = array() ) {
      parent::__construct(
        array(
          'singular' => 'Город',
          'plural'   => 'Города',
          'screen'   => isset( $args['screen'] ) ? $args['screen'] : 'sdaykat-api',
        )
      );
      $this->apiClass = new Sdaykat_Api_Get_Data();
    }

    public function get_columns(){
        $columns = array(
          'cb' => '<input type="checkbox" />',
          'name'      => 'Название',
          'region'    => 'Регион',
          // 'hide'      => 'Удалён'
        );
        return $columns;
    }

    public function no_items() {
      echo 'Города не найдены';
    }

    protected function get_bulk_actions() {
      $actions = array();
  
      $actions['delete'] = 'Удалить';
      
      return $actions;
    }

    protected function extra_tablenav( $which ) {
		?>
      <div class="alignleft actions">

        <label for="region_filter" class="screen-reader-text"><?php echo 'Фильтр по региону'; ?></label>
        <select id="region_filter" name="region_filter">
          <option value=""><?php echo 'Фильтр по региону'; ?></option>
          <?
          $regions = $this->apiClass->getRegions();
          $regions = $regions['regions'];
          foreach ($regions as $regionKey => $region) {
            echo '<option value="'. $region['id'] .'">'. $region['name'] .'</option>';
          }
          ?>
        </select>
        <?
        submit_button( 'Filter', '', 'filter', false);
        ?>
        </div> 
      <?
    }

    protected function get_sortable_columns() {
      $c = array(
        'name' => array('name',false),
        'region'    => array('region',false),
      );
  
      return $c;
    }

    public function display_rows_or_placeholder() {
      // Query the post counts for this page.
      if ( ! $this->is_site_users ) {
        $post_counts = count_many_users_posts( array_keys( $this->items ) );
      }
  
      foreach ( $this->items as $userid => $city_item ) {
        echo "\n\t" . $this->single_row( $city_item, '', '', isset( $post_counts ) ? $post_counts[ $userid ] : 0 );
      }
    }

    public function single_row( $city_item, $style = '', $role = '', $numposts = 0 ) {

      $region = $city_item['region']['name'];
      // $hideString = ( intval($city_item['hide']) ? 'да' : 'нет' ) ;

      $url =  ( $city_item['encoding'] ? $city_item['encoding'] : '' ) . '.' . $_SERVER['SERVER_NAME'];
  
      // Set up the hover actions for this user.
      $actions     = array();
      $checkbox    = '';
      $super_admin = '';
  
      // Set up the user editing link.
      $edit_link = 'edit_link';
      $edit            = "<strong><a href=\"{$edit_link}\">{$city_item['id']}</a>{$super_admin}</strong><br />";
      $actions['edit'] = '<a href="' . $edit_link . '">' . __( 'Edit' ) . '</a>';

      $actions['delete'] = "<a class='submitdelete' href='delete link'>" . __( 'Delete' ) . '</a>';

      $actions['view'] = sprintf(
        '<a href="%s" aria-label="%s">%s</a>',
        $url,
        esc_attr( sprintf( __( 'View posts by %s' ), $city_item['name'] ) ),
        __( 'View' )
      );

      // Set up the checkbox (because the user is editable, otherwise it's empty).
      $checkbox = sprintf(
        '<label class="screen-reader-text" for="user_%1$s">%1$s</label>' .
        '<input type="checkbox" name="cities[]" id="user_%1$s" value="%1$s" />',
        $city_item['id'],
        sprintf( __( 'Select %s' ), $city_item['name'] )
      );
      
      $r = '<tr id="'. $city_item["id"] . '">';

      list( $columns, $hidden, $sortable, $primary ) = $this->get_column_info();

      foreach ( $columns as $column_name => $column_display_name ) {
        if ( $primary === $column_name ) {
          $classes .= ' has-row-actions column-primary';
        }

        if ( 'cb' === $column_name ) {
          $r .= "<th scope='row' class='check-column'>$checkbox</th>";
        } else {
          $r .= "<td>";
          switch($column_name) {
            case 'name':
              $r .= $city_item['name'];
              break;
            case 'region':
              $r .= $region;
              break;
            // case 'hide':
            //   $r .= $hideString;
            //   break;
          }

          if ( $primary === $column_name ) {
            $r .= $this->row_actions( $actions );
          }
          $r .= '</td>';
        }
      }

      $r .= '</tr>';
  
      return $r;
    }
    
    public function prepare_items() {
      
      $columns = $this->get_columns();
      $hidden = array();
      $sortable = array();
      $this->_column_headers = array($columns, $hidden, $sortable);

      $this->process_bulk_action();
      global $search_string;

      $search_string = isset( $_REQUEST['s'] ) ? wp_unslash( trim( $_REQUEST['s'] ) ) : '';

      $per_page       = 20;
      $citys_per_page = $this->get_items_per_page( $per_page );

      $paged = $this->get_pagenum();

      $args = array(
        'number' => $citys_per_page,
        'offset' => ( $paged - 1 ) * $citys_per_page,
        'search' => $search_string,
        'fields' => 'all_with_meta',
      );

      if ( '' !== $args['search'] ) {
        $args['search'] = $args['search'];
      }

      if ( isset( $_REQUEST['orderby'] ) ) {
        $args['orderby'] = $_REQUEST['orderby'];
      }

      if ( isset( $_REQUEST['order'] ) ) {
        $args['order'] = $_REQUEST['order'];
      }

      // получаем данные
      $cities = $this->apiClass->getCities($args);
      $this->items = $cities['cities'];

      $this->set_pagination_args(
        array(
          'total_items' => $cities['global_count'],
          'per_page'    => $citys_per_page,
        )
      );
    }

    public function process_bulk_action() {

    do_action( 'sdaykat_api_send_form');
    // add_action( 'sdaykat_api_send_form', 'my_update_notice');
      //Detect when a bulk action is being triggered...
      if (isset($_REQUEST['region_filter'])) {
        var_dump($_REQUEST);
        // exit();

        // wp_redirect( esc_url( add_query_arg() ) );
        // exit;
      }
    
      // If the delete bulk action is triggered
      if ( ( isset( $_POST['action'] ) && $_POST['action'] == 'bulk-delete' )
           || ( isset( $_POST['action2'] ) && $_POST['action2'] == 'bulk-delete' )
      ) {
    
        $delete_ids = esc_sql( $_POST['bulk-delete'] );
    
        // loop over the array of record IDs and delete them
        foreach ( $delete_ids as $id ) {
          self::delete_customer( $id );
    
        }
    
        wp_redirect( esc_url( add_query_arg() ) );
        exit;
      }
    }
    
    public function column_default( $item, $column_name ) {
      switch( $column_name ) {
        case 'name':
        case 'region':
        case 'isbn':
          return $item[ $column_name ];
        default:
          return print_r( $item, true ) ; //Мы отображаем целый массив во избежание проблем
      }
    }
}