<?php


class Sdaykat_Api_Render {

    private $mode = 'admin';

    public function __construct($mode) {
        $this->mode = $mode;
    }

    public function tabs() {
        require plugin_dir_path( dirname( __FILE__ ) ) . $this->mode . '/partials/tabs.php';
    }

    public function geo_page() {
        require plugin_dir_path( dirname( __FILE__ ) ) . $this->mode . '/partials/geo_page.php';
    }

    public function regions_page() {
        require plugin_dir_path( dirname( __FILE__ ) ) . $this->mode . '/partials/regions_page.php';
    }

}