<?

class Sdaykat_Api_Get_Data {
    
    private $token;

    public function __construct() {
        $this->token = get_option('sdaykat-api-settings')['token'];
    }

    public function getCities($data = null) {
        return $this->api('system.getCitysPlus', $data);
    }

    public function getRegions($data = null) {
        return $this->api('system.getRegions', $data);
    }

    private function api($route, $data) {
        // 1. инициализация
        $ch = curl_init();

        $sendData = $data;
        $sendData['devToken'] = $this->token;
        $sendData['v'] = 4;

        // 2. указываем параметры, включая url
        curl_setopt($ch, CURLOPT_URL, "https://sdaykat.ru/api_vue/?" . $route);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_POSTFIELDS, $sendData);

        $response = curl_exec($ch);
        $response = json_decode($response, true);
        // if (json_decode($response, TRUE)["result"]["status"] != 'success') {
        // }

        // 4. закрываем соединение
        curl_close($ch);

        return $response['result']['data'];
    }
}